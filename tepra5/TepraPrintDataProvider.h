/**
 * @file TepraPrintDataProvider.h
 * @brief TEPRA-Print SDK TepraPrintDataProvider Class definition
 * @par Copyright:
 * (C) 2013 KING JIM CO.,LTD.<BR>
 */

#import <Foundation/Foundation.h>

/**
 * TepraPrintDataProvider
 * @brief Provider of contents data
 */
@interface TepraPrintDataProvider : NSObject

/**
 * beginJob of Application is processed.
 */
- (void)startOfPrint;

/**
 * endJob of Application is processed.
 */
- (void)endOfPrint;

/**
 * beginPage of Application is processed.
 */
- (void)startPage;

/**
 * endPage of Application is processed.
 */
- (void)endPage;

/**
 * number of pages to print is returned.
 * @return  number of pages
 */
- (NSInteger)numberOfPages;

/**
 * Form data is returned.
 * @param pageIndex Page number to print
 * @return form data
 */
- (NSDictionary *)formDataForPage:(NSInteger)pageIndex;

/**
 * Content data is returned.
 * @param contentName content name
 * @param pageIndex Page number to print
 * @return form data
 */
- (id)contentData:(NSString *)contentName forPage:(NSInteger)pageIndex;

@end
