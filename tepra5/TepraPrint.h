/**
 * @file    TepraPrint.h
 * @brief   TEPRA-Print SDK TepraPrint Class definition
 * @par     Copyright:
 * (C) 2013-2018 KING JIM CO.,LTD.<BR>
 */

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TepraPrintTapeOperation) {
    TapeOperationFeed = 0,
    TapeOperationFeedAndCut = -1
};

typedef NS_ENUM(NSInteger, TepraPrintTapeWidth) {
    TapeWidthNone = 0,
    TapeWidth4mm = 1,
    TapeWidth6mm = 2,
    TapeWidth9mm = 3,
    TapeWidth12mm = 4,
    TapeWidth18mm = 5,
    TapeWidth24mm = 6,
    TapeWidth36mm = 7,
    TapeWidth24mmCable = 8,
    TapeWidth36mmCable = 9,
    TapeWidth50mm = 10,
    TapeWidth100mm = 11,
    TapeWidthUnknown = -1
};


typedef NS_ENUM(NSInteger, TepraPrintTapeKind) {
    TapeKindNormal = 0
};

typedef NS_ENUM(NSInteger, TepraPrintPrintingPhase) {
    PrintingPhasePrepare = 1,
    PrintingPhaseProcessing = 2,
    PrintingPhaseWaitingForPrint = 3,
    PrintingPhaseComplete = 4
};

typedef NS_ENUM(NSInteger, TepraPrintConnectionStatus) {
    ConnectionStatusNoError = 0,
    ConnectionStatusConnectionFailed = -1,     /* Connection failed */
    ConnectionStatusDisconnected = -2,         /* Disconnect */
    ConnectionStatusDeviceBusy = -3,           /* Printer busy */
    ConnectionStatusOutOfMemory = -4,          /* Out of memory */
    ConnectionStatusDeviceError = -5,          /* Device error */
    ConnectionStatusCommunicationError = -6    /* Communication error */
};

typedef NS_ENUM(NSUInteger, TepraPrintStatusError) {
    StatusErrorNoError = 0x00,
    StatusErrorCutterError = 0x01,
    StatusErrorNoTapeCartridge = 0x06,
    StatusErrorHeadOverheated = 0x15,
    StatusErrorPrinterCancel = 0x20,
    StatusErrorCoverOpen = 0x21,
    StatusErrorLowVoltage = 0x22,
    StatusErrorPowerOffCancel = 0x23,
    StatusErrorTapeEjectError = 0x24,
    StatusErrorTapeFeedError = 0x30,
    StatusErrorInkRibbonSlack = 0x40,
    StatusErrorInkRibbonShort = 0x41,
    StatusErrorTapeEnd = 0x42,
    StatusErrorCutLabelError = 0x43,
    StatusErrorTemperatureError = 0x44,
    StatusErrorInsufficientParameters = 0x45,
    
    StatusErrorConnectionFailed = 0xfffffff0,
    
    StatusErrorOtherUsing = 0xfffffffa,
    StatusErrorFirmwareUpdating = 0xfffffffb,
    StatusErrorDeviceUsing = 0xfffffffc,
    
    StatusErrorUnknownError = 0xffffffff
    
};

typedef NS_ENUM(NSInteger, TepraPrintTapeCut) {
    TapeCutEachLabel = 0,
    TapeCutAfterJob = 1,
    TapeCutNotCut = 2
};

extern NSString *const TepraPrintParameterKeyCopies;        /* 1 ... 99 */
extern NSString *const TepraPrintParameterKeyTapeCut;       /* TepraPrintParameterTapeCut */
extern NSString *const TepraPrintParameterKeyHalfCut;       /* YES:Half cut (BOOL) */
extern NSString *const TepraPrintParameterKeyPrintSpeed;    /* YES:Low speed (BOOL) */
extern NSString *const TepraPrintParameterKeyDensity;       /* -5 ... 5 */
extern NSString *const TepraPrintParameterKeyTapeWidth;     /* TepraPrintTapeWidth */

extern NSString *const TepraPrintStatusKeyTapeKind;
extern NSString *const TepraPrintStatusKeyTapeWidth;
extern NSString *const TepraPrintStatusKeyDeviceError;

@protocol TepraPrintDelegate;
@class TepraPrintDataProvider;

/**
 * TepraPrint
 * @brief   Management of a printer (print/status/tape cut/etc.)
 */
@interface TepraPrint : NSObject

/**
 * set delegate
 */
@property (nonatomic, weak) id<TepraPrintDelegate> delegate;

/**
 * job progress
 * @return  Progress of a print
 */
@property(nonatomic, readonly) float progressOfPrint;
/**
 * page number of printing
 * @return  page number
 */
@property(nonatomic, readonly)  NSInteger pageNumberOfPrinting;

/**
 * set printer information
 * @param    printerInfo    Printer information
 */
- (void)setPrinterInformation:(NSDictionary *)printerInformation;

/**
 * print for application
 * @param    dataProvider Data provider of Application
 * @param   printParameter print parameter
 * @li @c   TepraPrintParameterKeyCopies 1 ... 99
 * @li @c   TepraPrintParameterKeyTapeCut TepraPrintParameterTapeCut
 * @li @c   TepraPrintParameterKeyHalfCut YES:Half cut (BOOL)
 * @li @c   TepraPrintParameterKeyLowSpeed YES:Low speed (BOOL)
 * @li @c   TepraPrintParameterKeyDensity  -5 ... 5
 */
- (void)doPrint:(TepraPrintDataProvider *)dataProvider printParameter:(NSDictionary *)printParameter;

/**
 * tape feed and tape send
 * @param    mode feed or send
 * @li @c   TapeOperationFeed
 * @li @c   TapeOperationFeedAndCut
 */
- (void)doTapeFeed:(TepraPrintTapeOperation)mode;

/**
 * resume job
 */
- (void)resumeOfPrint;

/**
 * cancel job
 */
- (void)cancelPrint;

/**
 * get printers tatus
 * @return  NSDictionary status dictionary
 */
- (NSDictionary *)fetchPrinterStatus;

/**
 * get tape width from status dictionary
 * @param   status status dictionary
 * @return  TepraPrintTapeWidth
 */
- (TepraPrintTapeWidth)tapeWidthFromStatus:(NSDictionary *)status;

/**
 * get tape kind from status dictionary
 * @param   status status dictionary
 * @return  TepraPrintTapeKind
 */
- (TepraPrintTapeKind)tapeKindFromStatus:(NSDictionary *)status;

/**
 * get device error from status dictionary
 * @param   status status dictionary
 * @return  TepraPrintDeviceStatus
 */
- (TepraPrintStatusError)deviceErrorFromStatus:(NSDictionary *)status;

/**
 * tape width which can be used
 * @return  TepraPrintDeviceStatus
 */
- (NSArray *)kindOfTape;

/**
 * model name
 * @return  model name
 */
- (NSString *)modelName;

/**
 * printer resolution
 * @return  resolution (180/270/300/360)
 */
- (NSInteger)resolution;

/**
 * printer equipped with the half cut
 * @return YES:equipment
 */
- (BOOL)isSupportHalfCut;

/**
 * low-speed printing supported
 * @return YES:supported
 */
- (BOOL)isPrintSpeedSupport;

@end

/**
 * TepraPrintDelegate
 * @brief   TepraPrint delegate
 */
@protocol TepraPrintDelegate <NSObject>

@optional

/**
 * called when print phase changes
 * @param   tepraPrint caller
 * @param   jobPhase job phase TepraPrintJobPhase
 */
- (void)tepraPrint:(TepraPrint *)tepraPrint didChangePrintOperationPhase:(TepraPrintPrintingPhase)jobPhase;

/**
 * called when a print job is suspended
 * @param   tepraPrint caller
 * @param   errorStatus error status TepraPrintErrorStatus
 * @param   deviceStatus device status TepraPrintDeviceStatus
 */
- (void)tepraPrint:(TepraPrint *)tepraPrint didSuspendPrintOperation:(TepraPrintConnectionStatus)errorStatus deviceStatus:(TepraPrintStatusError)deviceStatus;

/**
 * called when a print job is aborted
 * @param   tepraPrint caller
 * @param   errorStatus error status TepraPrintErrorStatus
 * @param   deviceStatus device status TepraPrintDeviceStatus
 */
- (void)tepraPrint:(TepraPrint *)tepraPrint didAbortPrintOperation:(TepraPrintConnectionStatus)errorStatus deviceStatus:(TepraPrintStatusError)deviceStatus;

/**
 * called when tape feed phase changes
 * @param   tepraPrint caller
 * @param   jobPhase job phase TepraPrintJobPhase
 */
- (void)tepraPrint:(TepraPrint *)tepraPrint didChangeTapeFeedOperationPhase:(TepraPrintPrintingPhase)jobPhase;
/**
 * called when a tape feed is aborted
 * @param   tepraPrint caller
 * @param   errorStatus error status TepraPrintErrorStatus
 * @param   deviceStatus device status TepraPrintDeviceStatus
 */
- (void)tepraPrint:(TepraPrint *)tepraPrint didAbortTapeFeedOperation:(TepraPrintConnectionStatus)errorStatus deviceStatus:(TepraPrintStatusError)deviceStatus;

@end
