/**
 * @file ViewController.swift
 * @brief TepraPrintSampleSwift ViewController Class definition
 * @par Copyright:
 * (C) 2018 KING JIM CO.,LTD.<BR>
 */

import UIKit

class ViewController: UIViewController, TepraPrintDelegate, DiscoverTableViewControllerDelegate, UIAlertViewDelegate, UITextFieldDelegate
{
    // MARK: IBOutlet
    @IBOutlet weak var discoverButton: UIButton!
    @IBOutlet weak var dataTextField: UITextField!
    @IBOutlet weak var dataSegmentedControl: UISegmentedControl!
    @IBOutlet weak var printButton: UIButton!
    
    // MARK: private member
    fileprivate let SHARED_TepraPrint: TepraPrint = TepraPrint()
    fileprivate let DATA_PROVIDER: SampleDataProvider = SampleDataProvider()
    
    fileprivate var processing: Bool = false
    fileprivate var printerInfo: [String: AnyObject]?

    
    // MARK: override method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        dataTextField.text = sharedDataProvider().stringData
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        print("222222222222----branchtest------")
        discoverButton.setTitle("Select: Device is not selected", for: UIControl.State())
        if let printerInfo = printerInfo
        
        {
            print("4444444444")
            
            if let _bonjourName = printerInfo["BonjourName"] as? String
            {
                discoverButton.setTitle(String(format: "Select:%@", _bonjourName), for: UIControl.State())
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let _navigationController = segue.destination as? UINavigationController
        {
            if segue.identifier == "DiscoverPrinter"
            {
                dataTextField.resignFirstResponder()
                
                if let _viewController = _navigationController.topViewController as? DiscoverTableViewController
                {
                    _viewController.delegate = self
                    print("4")
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: IBAction
    @IBAction func doPrint(_ sender: AnyObject)
    {
        dataTextField.resignFirstResponder()
        setProcessing(true)
        
        performPrint()
    }
    
    @IBAction func dataSegmentChanged(_ sender: AnyObject)
    {
        if dataSegmentedControl.selectedSegmentIndex == 0
        {
            sharedDataProvider().qrCodeData = dataTextField.text
            dataTextField.text = sharedDataProvider().stringData
        }
        else if dataSegmentedControl.selectedSegmentIndex == 1
        {
            sharedDataProvider().stringData = dataTextField.text
            dataTextField.text = sharedDataProvider().qrCodeData
        }
    }
    
    // MARK: DiscoverTableViewControllerDelegate
    func discoverView(_ discoverView: DiscoverTableViewController, didSelectPrinter printerInfo: [String: AnyObject])
    {
        self.printerInfo = printerInfo
        print("555555555")
    }
    
    // MARK: TepraPrintDelegate
    func tepraPrint(_ tepraPrint: TepraPrint!, didChangePrintOperationPhase jobPhase: TepraPrintPrintingPhase)
    {
        // Report the change of a printing phase
        var phase = ""
        
        switch jobPhase
        {
        case TepraPrintPrintingPhase.PrintingPhasePrepare:
            phase = "PrintingPhasePrepare"
        case TepraPrintPrintingPhase.PrintingPhaseProcessing:
            phase = "PrintingPhaseProcessing"
        case TepraPrintPrintingPhase.PrintingPhaseWaitingForPrint:
            phase = "PrintingPhaseWaitingForPrint"
        case TepraPrintPrintingPhase.PrintingPhaseComplete:
            phase = "PrintingPhaseComplete"
            self.printComplete(TepraPrintConnectionStatus.ConnectionStatusNoError, deviceStatus: TepraPrintStatusError.StatusErrorNoError, isSuspend: false)
            self.setProcessing(false)
        }
        
        print("<TepraPrint(_:didChangePrintOperationPhase:)>phase=\(phase)")
    }
    
    func tepraPrint(_ tepraPrint: TepraPrint!, didAbortPrintOperation errorStatus: TepraPrintConnectionStatus, deviceStatus: TepraPrintStatusError)
    {
        // It is called when undergoing a transition to the printing cancel operation due to a printing error
        self.printComplete(errorStatus, deviceStatus: deviceStatus, isSuspend: false)
        
        self.setProcessing(false)
        
        let message = String(format: "Error Status : %d\nDevice Status : %02X", errorStatus.rawValue, deviceStatus.rawValue)
        let alert = UIAlertView(title: "Print Error!", message: message, delegate: nil, cancelButtonTitle: "OK")
        alert.show()
    }
    
    func tepraPrint(_ tepraPrint: TepraPrint!, didSuspendPrintOperation errorStatus: TepraPrintConnectionStatus, deviceStatus: TepraPrintStatusError)
    {
        // It is called when undergoing a transition to the printing restart operation due to a printing error
        self.printComplete(errorStatus, deviceStatus: deviceStatus, isSuspend: true)
        
        let message = String(format: "Error Status : %d\nDevice Status : %02X", errorStatus.rawValue, deviceStatus.rawValue)
        let alert = UIAlertView(title: "Print Error! re-print ?", message:message, delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "OK")
        alert.show()
    }
    
    // MARK: UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if dataSegmentedControl.selectedSegmentIndex == 0
        {
            sharedDataProvider().stringData = dataTextField.text
        }
        else if dataSegmentedControl.selectedSegmentIndex == 1
        {
            sharedDataProvider().qrCodeData = dataTextField.text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: UIAlertViewDelegate
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int)
    {
        let TepraPrint = sharedTepraPrint()
        if buttonIndex == 0
        {
            TepraPrint.cancel()
        }
        else
        {
            TepraPrint.resumeOfPrint()
        }
    }
    
    // MARK: public method
    func getProcessing() -> Bool
    {
        return processing
    }
    
    // MARK: private method
    fileprivate func performPrint()
    {
        guard let printerInfo = self.printerInfo else
        
            
        {
            setProcessing(false)
            print("111111111111111")
            print(self.printerInfo as Any)
            
            return
        }
        
        DispatchQueue.global(qos: .default).async { () -> Void in
            
            let TepraPrint = self.sharedTepraPrint()
            
            // Set printing information
            TepraPrint.setPrinterInformation(printerInfo)
            
            // Obtain printing status
            guard let lwStatus = TepraPrint.fetchPrinterStatus() else
            {
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    let message = String(format: "Can't get printer status.")
                    let alert = UIAlertView(title: "Error", message:message, delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                    
                    self.setProcessing(false)
                })
                return
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                
                let dataProvider = self.sharedDataProvider()
                if self.dataSegmentedControl.selectedSegmentIndex == 0
                {
                    dataProvider.formType = .string
                }
                else
                {
                    dataProvider.formType = .qrCode
                }
                
                // Make a print parameter
                let tapeWidth = TepraPrint.tapeWidth(fromStatus: lwStatus)
                TepraPrint.delegate = self
                
                let printParameter: [String: AnyObject] = [
                    TepraPrintParameterKeyCopies: 1 as AnyObject,                                           // Number of copies(1 ... 99)
                    TepraPrintParameterKeyTapeCut: TepraPrintTapeCut.TapeCutEachLabel.rawValue as AnyObject,   // Tape cut method(TepraPrintTapeCut)
                    TepraPrintParameterKeyHalfCut: true as AnyObject,                                       // Set half cut (true:half cut on)
                    TepraPrintParameterKeyPrintSpeed: false as AnyObject,                                   // Low speed print setting (true:low speed print on)
                    TepraPrintParameterKeyDensity: 0 as AnyObject,                                          // Print density(-5 ... 5)
                    TepraPrintParameterKeyTapeWidth: tapeWidth.rawValue as AnyObject,                       // Tape width(TepraPrintTapeWidth)
                ]
                
                // Carry out printing
                TepraPrint.do(dataProvider, printParameter: printParameter)
            })
        }
    }
    
    fileprivate func sharedTepraPrint() -> TepraPrint
    {
        return SHARED_TepraPrint
    }
    
    fileprivate func sharedDataProvider() -> SampleDataProvider
    {
        return DATA_PROVIDER
    }
    
    fileprivate func setProcessing(_ value: Bool)
    {
        processing = value
        
        if processing == true
        {
            discoverButton.isEnabled = false
            dataTextField.isEnabled = false
            dataSegmentedControl.isEnabled = false
            printButton.isEnabled = false
        }
        else
        {
            discoverButton.isEnabled = true
            dataTextField.isEnabled = true
            dataSegmentedControl.isEnabled = true
            printButton.isEnabled = true
        }
    }
    
    fileprivate func printComplete(_ connectionStatus: TepraPrintConnectionStatus, deviceStatus: TepraPrintStatusError, isSuspend: Bool)
    {
        let app = UIApplication.shared
        let appDelegate = app.delegate as! AppDelegate
        if appDelegate.bgTask == UIBackgroundTaskIdentifier.invalid
        {
            return
        }
        
        let device = UIDevice.current
        let backgroundSupported = device.isMultitaskingSupported
        if !backgroundSupported
        {
            return
        }
        
        var msg = ""
        if connectionStatus == TepraPrintConnectionStatus.ConnectionStatusNoError && deviceStatus == TepraPrintStatusError.StatusErrorNoError
        {
            msg = "Print Complete."
        }
        else
        {
            if isSuspend
            {
                msg = String(format: "Print Error Re-Print [%02x].", deviceStatus.rawValue)
            }
            else
            {
                msg = String(format: "Print Error [%02x].", deviceStatus.rawValue)
            }
        }
        
        let alarm = UILocalNotification()
        alarm.fireDate = Date()
        alarm.timeZone = TimeZone.current
        alarm.repeatInterval = NSCalendar.Unit(rawValue: 0)
        alarm.soundName = "alarmsound.caf"
        alarm.alertBody = msg
        app.scheduleLocalNotification(alarm)
    }
}

