/**
 * @file TepraPrintDiscoverPrinter.h
 * @brief TEPRA-Print SDK TepraPrintDiscoverPrinter Class definition
 * @par Copyright:
 * (C) 2013-2018 KING JIM CO.,LTD.<BR>
 */

typedef NS_OPTIONS(NSUInteger, TepraPrintDiscoverConnectionType) {
    ConnectionTypeAll           = 0,
    ConnectionTypeNetwork       = 1 << 0,
    ConnectionTypeBluetooth     = 1 << 1,
};

extern NSString * const TepraPrintPrinterInfoBonjourName;
extern NSString * const TepraPrintPrinterInfoType;
extern NSString * const TepraPrintPrinterInfoDomain;
extern NSString * const TepraPrintPrinterInfoMDL;
extern NSString * const TepraPrintPrinterInfoMFG;
extern NSString * const TepraPrintPrinterInfoProduct;
extern NSString * const TepraPrintPrinterInfoIPAddress;
extern NSString * const TepraPrintPrinterInfoBonjourHostName;

@protocol TepraPrintDiscoverPrinterDelegate;

/**
 * TepraPrintDiscoverPrinter
 * @brief Discovery of printers
 */
@interface TepraPrintDiscoverPrinter : NSObject {
@private
}

@property (nonatomic,weak)   id <TepraPrintDiscoverPrinterDelegate>delegate;

/**
 * init
 * @param    models models to search
 * @return  TepraPrintDiscoverPrinter object
 */
- (id)initWithModels:(NSArray *)models connectionType:(TepraPrintDiscoverConnectionType)connectionType;

/**
 * start discover
 */
- (void)startDiscover;

/**
 * stop discover
 */
- (void)stopDiscover;

@end

/**
 * TepraPrintDiscoverPrinterDelegate
 * @brief TepraPrintDiscoverPrinter delegate
 */
@protocol TepraPrintDiscoverPrinterDelegate <NSObject>
@optional

/**
 * called when a printer is discovered.
 * @param   discoverPrinter caller
 @ @param   preinters discovered printer
 */
- (void)discoverPrinter:(TepraPrintDiscoverPrinter *)discoverPrinter didFindPrinter:(NSDictionary *)printerInformation;

/**
 * called when a printer is removed.
 * @param   discoverPrinter caller
 @ @param   preinters removed printer
 */
- (void)discoverPrinter:(TepraPrintDiscoverPrinter *)discoverPrinter didRemovePrinter:(NSDictionary *)printerInformation;

@end


