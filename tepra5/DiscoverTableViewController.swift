/**
* @file DiscoverTableViewController.swift
* @brief TepraPrintSampleSwift DiscoverTableViewController Class definition
* @par Copyright:
* (C) 2018 KING JIM CO.,LTD.<BR>
*/


import UIKit

@objc protocol DiscoverTableViewControllerDelegate: NSObjectProtocol
{
    @objc optional func discoverView(_ discoverView: DiscoverTableViewController, didSelectPrinter printerInfo: [String: AnyObject])
    
}

class DiscoverTableViewController: UITableViewController, TepraPrintDiscoverPrinterDelegate
{
    // MARK: public member
    var delegate: DiscoverTableViewControllerDelegate?
    
    // MARK: private member
    fileprivate let CELL_IDENTIFIER = "DiscoverPrinteCell"
    
    fileprivate var discover: TepraPrintDiscoverPrinter!
    fileprivate var printers: [[String : AnyObject]]!
    
    // MARK: override method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if printers == nil
        {
            printers = Array()
        }
        
        // Create the object for TepraPrintDiscoverPrinter
        if discover == nil
        {
            // models:  Specify the printing model name of retrieval object with array
            //          In terms of nil, all printers compatible would be object retrieval
            // connectionType:  Specify the printing type of searching object with TepraPrintDiscoverConnectionType
            print("discover == nil")
            discover = TepraPrintDiscoverPrinter(models: nil, connectionType: TepraPrintDiscoverConnectionType())
            discover.delegate = self
        }
        // Begin search for a printer
        discover.startDiscover()
        print("111")
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        print("112")
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("113")
        print(printers.count)
        return printers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        print("114")
        var cell: UITableViewCell! = nil
        let printer = printers[indexPath.row]
        print(printer)
        cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER)
        if cell == nil
        {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: CELL_IDENTIFIER)
        }
        cell.detailTextLabel?.text = printer[TepraPrintPrinterInfoType] as? String
        cell.textLabel?.text = printer[TepraPrintPrinterInfoBonjourName] as? String
        
        return cell
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("115")
        let printerInfo = printers[indexPath.row]
        print("333333333")
        print(printerInfo)
        if let _delegate = delegate
        {
            _delegate.discoverView?(self, didSelectPrinter: printerInfo)
        }
        // Finish search for a printer
        discover.stopDiscover()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: TepraPrintDiscoverPrinterDelegate
    func discoverPrinter(_ discoverPrinter: TepraPrintDiscoverPrinter!, didFindPrinter printerInformation: [AnyHashable: Any]!)
    {
        print("116")
        // Notify when finding out a printer
        guard let findPrinterInformation = printerInformation else
        {
            return
        }
        
        printers.append(findPrinterInformation as! [String : AnyObject])
        self.tableView.reloadData()
    }
    
    func discoverPrinter(_ discoverPrinter: TepraPrintDiscoverPrinter!, didRemovePrinter printerInformation: [AnyHashable: Any]!)
    {
        print("117")
        // Notify when loosing sight of a printer
        guard let removePrinterInformation = printerInformation else
        {
            return
        }
        
        for (index, registeredInformation) in printers.enumerated()
        {
            if (removePrinterInformation as NSDictionary).isEqual(to: registeredInformation)
            {
                printers.remove(at: index)
                break
            }
        }
        
        self.tableView.reloadData()
    }
    
    // MARK: IBAction
    @IBAction func doneButton(_ sender: AnyObject)
    {
        // Finish search for a printer
        discover.stopDiscover()
        self.dismiss(animated: true, completion: nil)
    }
}
